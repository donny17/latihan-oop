<?php

require_once ('animal.php');
require ('frog.php');
require ('ape.php');

$sheep = new Animal("shaun");

echo "Name: " . $sheep->name . "<br>" ; // "shaun"
echo "Legs: " . $sheep->legs . "<br>"; // 4
echo "Cold Blooded: " . $sheep->cold_blooded. "<br> <br>"; // "no"



$buduk = new frog ("Buduk");
echo "Name: " . $buduk->name . "<br>";
echo "Legs: " . $buduk->legs . "<br>";
echo "Cold Blooded: " . $buduk->cold_blooded . "<br>" ; 
$buduk->jump() ;


$sungokong = new Ape ("Kera Sakti");
echo "Name: " . $sungokong->name . "<br>";
echo "Legs: " . $sungokong->legs . "<br>";
echo "Cold Blooded: " . $sungokong->cold_blooded . "<br>" ; 
$sungokong->yell();

 
?>